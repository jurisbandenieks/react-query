import { useQuery } from "react-query";
import Planet from "./Planet";
import { useState } from "react";

const fetchPlanets = async ({ queryKey }) => {
  const [_key, { page }] = queryKey;
  const res = await fetch(`http://swapi.dev/api/planets?page=${page}`);
  return res.json();
};

const Planets = () => {
  const [page, setPage] = useState(1);
  const { data, isError, isLoading } = useQuery(
    ["planets", { page }],
    fetchPlanets,
    { keepPreviousData: true, staleTime: 5000 }
  );

  if (isLoading) return <div>Loading...</div>;
  if (isError) return <div>Something went wrong!</div>;

  return (
    <>
      <button
        disabled={page === 1}
        onClick={() => setPage((old) => Math.max(old - 1, 1))}
      >
        Previous
      </button>
      <span>{page}</span>
      <button
        disabled={!data || !data.next}
        onClick={() => setPage((old) => (!data || !data.next ? old : old + 1))}
      >
        Next
      </button>
      <button onClick={() => setPage(1)}>Page 1</button>
      <button onClick={() => setPage(2)}>Page 2</button>
      <button onClick={() => setPage(3)}>Page 3</button>

      {data.results.map((planet) => (
        <Planet key={planet.name} planet={planet} />
      ))}
    </>
  );
};

export default Planets;
